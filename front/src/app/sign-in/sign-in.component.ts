import { Component, OnInit } from '@angular/core';

import {FormControl, Validators} from '@angular/forms';
import { HttpService } from '../../services/http/http.service';
import { LocalStorageService } from '../../services/storages/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],

})

export class SignInComponent implements OnInit {
  email = "";
  password = '';
  error: string | undefined ='';
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  passwordFormControl = new FormControl('', [Validators.required]);
  // matcher = new MyErrorStateMatcher();

  constructor(
    public http: HttpService,
    private storage: LocalStorageService,
    private router: Router) { }

  ngOnInit(): void {
  }

  async onSubmit() {
    const data = await this.http.signInRequest(this.email, this.password);
    if (data.result.token) {
      console.log(data)
      this.storage.setToken(`Bearer ${data.result.token}`);
      await this.router.navigate(['dashboard']);
    } else {
      this.error=data.result.error
    }

    console.log(data)
  }
}




