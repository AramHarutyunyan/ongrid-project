import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { HttpService } from '../../services/http/http.service';
import { LocalStorageService } from '../../services/storages/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  email = "";
  password = '';
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  passwordFormControl = new FormControl('', [Validators.required]);
  error: string | undefined ='';

  constructor(
    public http: HttpService,
    private storage: LocalStorageService,
    private router: Router) { }

  ngOnInit(): void {
  }

  async onSubmit() {
    const data = await this.http.signUpRequest(this.email, this.password);
    if(data.result === true) {
      await this.router.navigate(['signin']);
    } else {

    }
  }

}
