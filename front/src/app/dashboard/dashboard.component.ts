import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../services/storages/local-storage.service';
import { Router } from '@angular/router';
import * as Highcharts from 'highcharts';
import { HttpService } from '../../services/http/http.service';
import { Chart } from 'angular-highcharts';

export interface PeriodicElement {
  name: string;
  date: string;
  shipTo: string;
  paymentMethod: string;
  saleAmount: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  token='';
  highcharts = Highcharts;
  chartOptions: Highcharts.Options = {}
  displayedColumns: string[] = ['date', 'name', 'ship to', 'payment method', 'sale amount'];
  dataChart: number[] = [];
  dataSource: PeriodicElement[] = [];
  public chart: Chart | undefined;

  constructor(
    private storage: LocalStorageService,
    private router: Router,
    public http: HttpService,
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.token = this.storage.getToken();
    if(!this.token) {
      await this.router.navigate(['signin']);
    }

    const data = await this.http.paymentsRequest(this.token, true );
    if (data) {
      this.dataSource = [];
      data.result.forEach(surce => {
        this.dataChart.push(Number(surce.saleAmount));
        this.dataSource.push(surce);
      })
    }
    this.allChartOptions();
    // @ts-ignore
    this.chartOptions.series[0].data = this.dataChart;
    this.chart = new Chart(this.chartOptions);
  }

  allChartOptions() {
    this.chartOptions={
      chart: {
        type: "spline"
      },
      title: {
        text: "Today"
      },
      subtitle: {
        text: "i'm only BackEnd Developer"
      },
      xAxis:{
        categories:["03:00", "06:00", "09:00", "12:00", "15:00", "18:00", "21:00", "24:00"]
      },
      yAxis: {
        title:{
          text:"Sales ($)"
        }
      },
      tooltip: {
        valueSuffix:" °C"
      },
      series: [
        {
          name: 'Recent Deposits $3,024.00',
          type: 'line',
          data: this.dataChart
        },
      ]
    };
  }

}
