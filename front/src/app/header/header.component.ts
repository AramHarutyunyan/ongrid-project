import {Component, Input, OnInit} from '@angular/core';
import {LocalStorageService} from "../../services/storages/local-storage.service";
import {Router} from "@angular/router";
import {HttpService} from "../../services/http/http.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input()
  show: boolean | undefined;
  constructor(private storage: LocalStorageService,
              private router: Router,
              public http: HttpService,) { }

  ngOnInit(): void {
  }


  async logOut(): Promise<void> {
    await this.storage.destroyToken();
    await this.router.navigate(['signin']);
  }
}
