import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


export interface Payment {
  id?: bigint;
  name: string;
  date: string;
  shipTo: string;
  paymentMethod: string;
  saleAmount: string;
  createdAt?: Date;
}

export interface SignInInterface {
  result: {
    token?: string;
    error?:string
  }
}

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  authAddress = 'http://127.0.0.1:7001/api/auth';
  dashboardAddress = 'http://127.0.0.1:7001/api/dashboard';
  constructor(private http: HttpClient) { }

  async signInRequest(email: string, password: string): Promise<SignInInterface> {
    return new Promise(async(resolve, reject) => {
      await this.http.post<SignInInterface>(`${this.authAddress}/signin`, { email, password }).subscribe((data) => {
        if(data.result) {
          resolve(data);
        }
      }, error => {
        console.log(error);
        resolve({
          result:{
            error: "Authentication didn’t pass."
          }
        });
      })

    })
  }

 async signUpRequest(email: string, password: string): Promise<{result: boolean}> {
    return new Promise(async(resolve, reject) => {
      await this.http.post<{result: boolean}>(`${this.authAddress}/signup`, { email, password }).subscribe(data => {
        resolve(data);
      }, error => {
          console.log(error);
          reject(error);
        })
    })
  }

  async paymentsRequest(token: string, allData: boolean): Promise<{result: Array<Payment>}>  {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${token}`
    });

    const requestOptions = { headers };

    return new Promise(async(resolve, reject) => {
      await this.http.get<{result: Array<Payment>}>(`${this.dashboardAddress}/my-page?allData=${allData}`, requestOptions).subscribe(data => {
        resolve(data);
      },
        error => {
        reject(error)
        });
    })
  }
}
