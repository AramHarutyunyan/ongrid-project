export interface User {
  id?: bigint;
  name: string;
  nickName: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}

export interface Payment {
  id?: bigint;
  name: string;
  date: string;
  shipTo: string;
  paymentMethod: string;
  saleAmount: string;
  createdAt?: Date;
}
