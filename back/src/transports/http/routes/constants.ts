export enum different {
  defaultLimit = 2,
  maxFileSize = 8388608,
  metaCharacterLimit = 32,
}
