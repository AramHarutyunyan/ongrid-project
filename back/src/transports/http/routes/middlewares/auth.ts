/* eslint-disable no-unused-vars */
import { Logger as PinoLogger } from 'pino';
import { Response, NextFunction } from 'express';
import { Logger } from '../../../../libraries/logger/logger';
import { Request } from '../../../../libraries/interfaces/http';

import { MiddlewareHandler } from './interface';
import {
  AuthorizationHeaderRequired,
} from '../../../../libraries/errors/authorization-header-required';
import { InvalidJwtToken } from '../../../../libraries/errors/invalid-jwt-token';
import { Services } from '../../../../services';
import { JwtValidationFailed } from '../../../../libraries/errors/jwt-validation-failed';

interface VerifiedResult {
  userId?: bigint;
  error?: string;
}

export class AuthMiddleware implements MiddlewareHandler {
  private readonly logger: PinoLogger;

  private readonly services: Services;

  constructor(services: Services, logger: Logger) {
    this.services = services;
    this.logger = logger.getLogger('auth-middleware');
  }

  public async handler(
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> {
    const header = req.header('Authorization');

    if (!header) {
      return next(new AuthorizationHeaderRequired());
    }

    const [type, token] = header.split(' ');

    if (type !== 'Bearer' || !token || token === '') {
      return next(new InvalidJwtToken());
    }

    const result = await new Promise<VerifiedResult>(async (resolve) => {
      const result = await this.services.auth.verify({ token });

      if (result.error) {
        return next(new JwtValidationFailed(result.error));
      }

      if (result.id) {
        req.body.userId = result.id;
      }

      return next();
    });
  }
}
