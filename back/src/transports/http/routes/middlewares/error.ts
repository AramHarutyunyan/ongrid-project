/* eslint-disable no-unused-vars */
import { Logger as PinoLogger } from 'pino';
import { Response, NextFunction } from 'express';

import { Logger } from '../../../libraries/logger/logger';
import { ApplicationError } from '../../../libraries/errors';
import { Request } from '../../../libraries/interfaces/http';

import { MiddlewareHandler } from './interface';

export class ErrorMiddleware implements MiddlewareHandler {
  private readonly logger: PinoLogger;

  constructor(logger: Logger) {
    this.logger = logger.getLogger('error-middleware');
  }

  public handler(
    err: Error,
    req: Request,
    res: Response,
    next: NextFunction,
  ): void {
    this.logger.warn(err, 'error handler fire');

    if (err instanceof ApplicationError) {
      res.statusCode = err.getStatusCode();
      res.json(
        {
          error: err.message,
        },
      );
    } else {
      res.statusCode = 500;
      res.json({
        error: 'internal server error',
        details: err.message,
      });
    }
  }
}
