export interface SignInResponse {
  result: {
    token: string
  };
}

export interface SigUpResponse {
  result: boolean;
}
