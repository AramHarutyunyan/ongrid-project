import { Logger as PinoLogger } from 'pino';
import {
  NextFunction,
  Request as ExpressRequest,
  Response as ExpressResponse,
  Router,
} from 'express';

import { Logger } from '../../../../libraries/logger/logger';

import { Services } from '../../../../services';
import { SignInParams } from '../../../../services/auth/interfaces';

import {
  PaymentsResponse,
  SigUpResponse,
} from './interfaces';
import { MyPageParams } from '../../../../services/dashboard/interfaces';

export class DashboardRoute {
  private readonly logger: PinoLogger;

  private readonly services: Services;

  constructor(services: Services, logger: Logger) {
    this.logger = logger.getLogger('collect-route');
    this.services = services;
  }

  public init(): Router {
    this.logger.info('Auth route init');
    const router = Router();
    router
      .get(
        '/my-page',
        async (
          ...args
        ) => this.myPage(...args),
      );

    return router;
  }

  private async myPage(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      if (
        !req.query.allData
      ) {
        throw new Error('Username or password required');
      }

      const params: MyPageParams = {
        allData: !!req.query.allData || true,
      };

      const paymentsData = await this.services.dashboard.myPage(params);
      const response: PaymentsResponse = {
        result: paymentsData,
      };

      return res.json(response);
    } catch (e) {
      return next(e);
    }
  }

  private async signUp(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      if (
        !req.body.userName
        || req.body.userName === ''
        || !req.body.password
        || req.body.password === ''
      ) {
        throw new Error('Username or password required');
      }

      const params: SignInParams = {
        userName: req.body.userName,
        password: req.body.password,
      };

      const isSignUp = await this.services.auth.signUp(params);
      const response: SigUpResponse = {
        result: isSignUp,
      };

      return res.json(response);
    } catch (e) {
      return next(e);
    }
  }
}
