import { Payment as PaymentDomain } from '../../../../domains/user';

export interface SignInResponse {
  result: {
    token: string
  };
}

export interface SigUpResponse {
  result: boolean;
}

export interface PaymentsResponse {
  result: Array<PaymentDomain> | undefined;
}
