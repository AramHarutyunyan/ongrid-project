/* eslint-disable no-unused-vars */
import path from 'path';

import { Logger as PinoLogger } from 'pino';
import {
  Express,
  Request as ExpressRequest,
  Response as ExpressResponse,
  NextFunction,
} from 'express';
import { json } from 'body-parser';
import cors from 'cors';
import fileUpload from 'express-fileupload';

import { Logger } from '../../../libraries/logger/logger';
import { Request } from '../../../libraries/interfaces/http';
import { Config } from '../../../libraries/interfaces/config';

import { Services } from '../../../services';

import { ErrorMiddleware } from '../middlewares/error';
import { AuthRoute } from './auth';
import { DashboardRoute } from './dashboard';
import { AuthMiddleware } from './middlewares/auth';

export class Routes {
  private readonly log: PinoLogger;

  private readonly app: Express;

  private readonly logger: Logger;

  private readonly services: Services;

  private readonly config: Config;

  constructor(
    app: Express,
    services: Services,
    config: Config,
    logger: Logger,
  ) {
    this.log = logger.getLogger('root-router');
    this.app = app;
    this.logger = logger;
    this.services = services;
    this.config = config;
  }

  public init(): void {
    const errorMiddleware = new ErrorMiddleware(this.logger);
    const authRoute = new AuthRoute(this.services, this.logger);
    const dashboardRoute = new DashboardRoute(this.services, this.logger);

    this.app.use(fileUpload({
      createParentPath: true,
    }));

    this.app.use(cors());
    this.app.use(json());

    this.app.get(
      '/docs/swagger',
      (
        req: ExpressRequest,
        res: ExpressResponse,
        next: NextFunction,
      ) => {
        res.sendFile(path.join(__dirname, '..', '..', '..', '..', 'api', 'api.json'));
      },
    );

    const authMiddleware = new AuthMiddleware(this.services, this.logger);

    this.app.use(async (
      req: ExpressRequest,
      res: ExpressResponse,
      next: NextFunction,
    ) => {
      const newReq = req as Request;

      const [first, secAddress, realAddress] = newReq.path.split('/');
      if (realAddress === 'dashboard') {
        await authMiddleware.handler(newReq, res, next);
      } else {
        next();
      }
    });

    this.app.use(Routes.getPath('auth'), authRoute.init());
    this.app.use(Routes.getPath('dashboard'), dashboardRoute.init());
    this.app.use((
      err: Error,
      req: ExpressRequest,
      res: ExpressResponse,
      next: NextFunction,
    ) => {
      const newReq = req as Request;

      errorMiddleware.handler(err, newReq, res, next);
    });
  }

  private static getPath(path: string): string {
    return `/api/${path}`;
  }
}
