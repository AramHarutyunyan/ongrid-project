import { Server } from 'http';
import { Logger as PinoLogger } from 'pino';
import express, { Express } from 'express';
import { Config } from '../../libraries/interfaces/config';
import { Logger } from '../../libraries/logger/logger';
import { Services } from '../../services';

import { Routes } from './routes';

export class HttpServer {
  private readonly config: Config;

  private readonly log: PinoLogger;

  private readonly app: Express;

  private readonly logger: Logger;

  private readonly services: Services;

  private server?: Server;

  constructor(
    config: Config,
    services: Services,
    logger: Logger,
  ) {
    this.config = config;
    this.log = logger.getLogger('http-server');
    this.app = express();
    this.logger = logger;
    this.services = services;
  }

  public start() {
    this.log.info('starting http server');
    this.init();

    this.server = this.app.listen(
      this.config.listen.ports.http,
      this.config.listen.host,
      () => this.log
        .info(`server is starting hist ${this.config.listen.host} in port ${this.config.listen.ports.http}`),
    );
  }

  public close(): void {
    this.server?.close((err) => {
      if (err) {
        this.log.error(err);
      }

      this.log.info('server is closed');
    });
  }

  private init(): void {
    const root = new Routes(
      this.app,
      this.services,
      this.config,
      this.logger,
    );
    this.log.info('running instance class route');
    root.init();
  }
}
