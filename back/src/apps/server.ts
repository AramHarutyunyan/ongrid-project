/* eslint-disable no-unused-vars */
import { EventEmitter } from 'events';

export abstract class Server {
  public abstract run(root: string, emitter?: EventEmitter): Promise<void>

  public abstract stop(): Promise<void>
}
