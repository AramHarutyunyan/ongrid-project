import jwt from 'jsonwebtoken';

import { AppJwtConfig } from '../interfaces/config';
import { Payload } from './interfaces';

export class Jwt {
    private readonly config: AppJwtConfig;

    constructor(config: AppJwtConfig) {
      this.config = config;
    }

    public async sign(data: Payload): Promise<string | undefined> {
      return new Promise<string | undefined>((resolve, reject) => {
        jwt.sign(
          {
            data,
          },
          this.config.secret,
          {
            expiresIn: this.config.expire,
            algorithm: this.config.algorithm,
          },
          (err, token) => {
            if (err) {
              return reject(err);
            }

            return resolve(token);
          },
        );
      });
    }

    public async verify(token: string): Promise<Payload> {
      return new Promise<Payload>((resolve, reject) => {
        jwt.verify(
          token,
          this.config.secret,
          {
            algorithms: [
              this.config.algorithm,
            ],
          },
          (err, payload) => {
            if (err) {
              return reject(err);
            }

            return resolve(payload as Payload);
          },
        );
      });
    }
}
