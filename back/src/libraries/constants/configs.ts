import { ValidationSchema } from 'fastest-validator';
import { ValidationTypes } from './validation-types';

export const configsSchema: ValidationSchema = {
  app: {
    type: ValidationTypes.object,
    props: {
      jwt: {
        type: ValidationTypes.object,
        props: {
          secret: {
            type: ValidationTypes.string,
            empty: false,
          },
          expire: {
            type: ValidationTypes.number,
            positive: true,
          },
          algorithm: {
            type: ValidationTypes.string,
            empty: false,
          },
        },
      },
    },
  },
  logger: {
    type: ValidationTypes.object,
    props: {
      levels: {
        type: ValidationTypes.object,
      },
      prettry: {
        type: ValidationTypes.boolean,
      },
      sentry: {
        type: ValidationTypes.object,
        props: {
          dsn: {
            type: ValidationTypes.string,
            optional: true,
            empty: false,
          },
        },
      },
    },
  },
  listen: {
    type: ValidationTypes.object,
    props: {
      ports: {
        type: ValidationTypes.object,
        props: {
          http: {
            type: ValidationTypes.number,
            positive: true,
          },
        },
      },
      host: {
        type: ValidationTypes.string,
        empty: false,
      },
    },
  },
  db: {
    type: ValidationTypes.object,
    props: {
      seed: {
        type: ValidationTypes.object,
        props: {
          version: {
            type: ValidationTypes.string,
            empty: true,
            optional: true,
          },
        },
      },
      pg: {
        type: ValidationTypes.object,
        props: {
          version: {
            type: ValidationTypes.string,
            empty: false,
            optional: true,
          },
          host: {
            type: ValidationTypes.string,
            empty: false,
          },
          port: {
            type: ValidationTypes.number,
          },
          db: {
            type: ValidationTypes.string,
            empty: false,
          },
          user: {
            type: ValidationTypes.string,
            empty: false,
          },
          password: {
            type: ValidationTypes.string,
            empty: false,
          },
        },
      },
    },
  },
};
