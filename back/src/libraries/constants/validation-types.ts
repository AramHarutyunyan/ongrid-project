export enum ValidationTypes {
  object = 'object',
  string = 'string',
  boolean = 'boolean',
  number = 'number'
}
