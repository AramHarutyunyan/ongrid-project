/* eslint-disable no-unused-vars */
export enum HttpStatusCodes {
  none = 0,
  InternalServerError = 500,
  unauthorized = 401,
  badRequest = 400,
  ok = 200,
}
