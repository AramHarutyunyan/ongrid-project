import { ApplicationError } from './index';

export class ValidationError extends ApplicationError {
  code = 422;
}
