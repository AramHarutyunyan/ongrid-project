import { ServerError } from './index';
import { ServerErrorCodes } from './constants';

export class NotImplementMethod extends ServerError {
  constructor(code: ServerErrorCodes) {
    super('not implement method', code);
  }
}
