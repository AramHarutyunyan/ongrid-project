// eslint-disable-next-line max-classes-per-file
import { ApplicationErrorCodes } from './constants';
import { HttpStatusCodes } from '../constants/http-status-codes';

export abstract class ApplicationError extends Error {
  protected code = ApplicationErrorCodes.none;

  protected statusCode = HttpStatusCodes.none;

  public getCode(): ApplicationErrorCodes {
    return this.code;
  }

  public getStatusCode(): HttpStatusCodes {
    return this.statusCode;
  }
}

export abstract class ServerError extends Error {
  private readonly code: number;

  protected constructor(
    massage:string,
    code: number,
  ) {
    super(massage);
    this.code = code;
  }

  public getCode() {
    return this.code;
  }
}
