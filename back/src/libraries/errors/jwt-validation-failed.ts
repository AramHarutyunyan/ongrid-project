import { HttpStatusCodes } from '../constants/http-status-codes';

import { ApplicationError } from './index';
import { ApplicationErrorCodes } from './constants';

export class JwtValidationFailed extends ApplicationError {
  constructor(msg: string) {
    super(msg);

    this.code = ApplicationErrorCodes.jwtValidationFailed;
    this.statusCode = HttpStatusCodes.unauthorized;
  }
}
