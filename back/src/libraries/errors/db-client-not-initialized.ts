import { ServerError } from './index';
import { ServerErrorCodes } from './constants';

export class DBClientNotInitialized extends ServerError {
  constructor() {
    super(
      'db client not initialized',
      ServerErrorCodes.dbClientNotInitialized,
    );
  }
}
