import { HttpStatusCodes } from '../constants/http-status-codes';

import { ApplicationError } from './index';
import { ApplicationErrorCodes } from './constants';

export class InvalidJwtToken extends ApplicationError {
  constructor() {
    super('invalid JWT token');

    this.code = ApplicationErrorCodes.invalidJwtToken;
    this.statusCode = HttpStatusCodes.badRequest;
  }
}
