import { ApplicationError } from './index';

export class UnAuthorized extends ApplicationError {
  constructor() {
    super('UnAuthorized');
    this.code = 401;
  }
}
