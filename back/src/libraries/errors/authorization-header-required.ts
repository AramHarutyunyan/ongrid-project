import { HttpStatusCodes } from '../constants/http-status-codes';

import { ApplicationError } from './index';
import { ApplicationErrorCodes } from './constants';

export class AuthorizationHeaderRequired extends ApplicationError {
  constructor() {
    super('Authorization header is required');

    this.code = ApplicationErrorCodes.authorizationHeaderRequired;
    this.statusCode = HttpStatusCodes.badRequest;
  }
}
