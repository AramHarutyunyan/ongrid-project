/* eslint-disable no-unused-vars,camelcase */
import { MigrationType } from './migrations/constants';
import { MigrationData } from './migrations/interfaces';

export interface Database<T> {
  getClient(): T | undefined;
  close(): Promise<void>;
  init(): Promise<void>;
  getVersion(type: MigrationType): Promise<string>;
  getMigrations(
    dbVersion: string,
    currentVersion: string,
    type: MigrationType,
  ): Promise<Array<MigrationData>>;
  setVersion(version: string, type: MigrationType): Promise<void>;
  deleteVersion(version: string, type: MigrationType): Promise<void>;
}

export interface Migration {
  id: bigint;
  version: string;
  created_at: number;
}
