/* eslint-disable class-methods-use-this */
import { Logger } from 'pino';

import { AppConfig } from '../../interfaces/config';
import { BaseDatabase } from '../index';

export class BaseMigration {
  private readonly logger: Logger;

  protected readonly db: BaseDatabase;

  protected readonly config: AppConfig;

  constructor(client: BaseDatabase, logger: Logger, config: AppConfig) {
    this.logger = logger;
    this.db = client;
    this.config = config;
  }

  public async up(): Promise<void> {
    throw new Error('not implemented');
  }

  public async down(): Promise<void> {
    throw new Error('not implemented');
  }

  public getName(): string {
    throw new Error('not implemented');
  }
}
