import { PoolClient } from 'pg';

import { BaseMigration } from '../base';
import { Postgres } from '../../pg';

export class CreateAllTable extends BaseMigration {
  private readonly name = '1648756412472_8toyfmc9_create_all_table';

  public async down(): Promise<void> {
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query('DROP TABLE IF EXISTS users;');
      await db.query('DROP TABLE IF EXISTS payments;');
      await db.query('COMMIT');
    } catch (e) {
      await db.query('ROLLBACK');
    }
  }

  public async up(): Promise<void> {
    console.log('1648756412472_8toyfmc9_create_all_table');
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query(
        `
          CREATE TABLE IF NOT EXISTS users
          (
            id         SERIAL
                CONSTRAINT users_pk PRIMARY KEY,
            name       VARCHAR(60)  NOT NULL,
            nickname   VARCHAR(60)  NOT NULL,
            password   VARCHAR(100) NOT NULL,
            created_at BIGINT,
            updated_at BIGINT,
            deleted_at BIGINT NULL
          );
        `,
      );
      await db.query(
        `
          CREATE UNIQUE INDEX users_unique_nickname
            on public.users (nickname);
        `,
      );

      await db.query(
        `
          CREATE TABLE IF NOT EXISTS payments
          (
            id         SERIAL
                CONSTRAINT payments_pk PRIMARY KEY,
            name       VARCHAR(60)  NOT NULL,
            date   VARCHAR(60)  NOT NULL,
            shipTo   VARCHAR(100) NOT NULL,
            paymentMethod VARCHAR(100),
            saleAmount VARCHAR(100),
            created_at BIGINT
          );
        `,
      );

      await db.query('COMMIT');
      await db.query('BEGIN');
      await db.query(
        `
          INSERT INTO payments("name", "date", "shipto", "paymentmethod", "saleamount")
            VALUES
              ('Aram Harutyunyan', '12 Dec 2022', 'Yerevan', 'VISA ***** 1249', '712.44'),
              ('Xachatur Abovyan', '12 Dec 2022', 'Armenia', 'VISA ***** 1249', '866.99'),
              ('Raffi', '12 Dec 2022', 'Yerevan', 'VISA ***** 1249', '700.81'),
              ('Gabriel Coco Chanel', '12 Dec 2022', 'Gorgia', 'VISA ***** 1249', '654.39'),
              ('Isahak Nyuton', '12 Dec 2022', 'Yerevan', 'VISA ***** 1249', '212.79'),
              ('Karp Xachvanqyan', '12 Dec 2022', 'Montenegro', 'VISA ***** 1249', '100.5'),
              ('Eminem', '12 Dec 2022', 'Montenegro', 'VISA ***** 1249', '255.2'),
              ('Albert Abstain', '12 Dec 2022', 'Montenegro', 'VISA ***** 1249', '260.5')
              ;
        `,
      );
      await db.query('COMMIT');
    } catch (e) {
      console.error(e);
      await db.query('ROLLBACK');
    }
  }

  public getName(): string {
    return this.name;
  }
}
