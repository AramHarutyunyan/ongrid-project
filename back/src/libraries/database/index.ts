/* eslint-disable no-unused-vars,no-restricted-syntax */
import { Logger as PinoLogger } from 'pino';
import { Direction, MigrationType } from './migrations/constants';
import { Config } from '../interfaces/config';
import { Logger } from '../logger/logger';
import { MigrationData } from './migrations/interfaces';
import { BaseMigration } from './migrations/base';
import { NotImplementMethod } from '../errors/not-implement-method';
import { ServerErrorCodes } from '../errors/constants';

export class BaseDatabase {
  protected readonly config: Config;

  protected logger: PinoLogger;

  constructor(config: Config, logger: Logger) {
    this.config = config;
    this.logger = logger.getLogger('db-client');
  }

  public async migrate(type: MigrationType): Promise<boolean> {
    this.logger.info('starting migration');
    const dbVersion = await this.getVersion(type);
    const migrations = await this.getMigrations(
      dbVersion,
      this.config.db.pg.version,
      type,
    );

    for await (const migration of migrations) {
      if (migration.direction === Direction.up) {
        await this.setVersion(migration.version, type);
        await migration.instance.up();
        await BaseDatabase.delay();
      } else if (migration.direction === Direction.down) {
        await this.deleteVersion(migration.version, type);
        await migration.instance.down();
        await BaseDatabase.delay();
      }
    }

    this.logger.info('finish migration');

    return true;
  }

  public async init(): Promise<void> {
    throw new NotImplementMethod(
      ServerErrorCodes.notImplementDatabaseInitMethod,
    );
  }

  public async getVersion(type: MigrationType): Promise<string> {
    throw new NotImplementMethod(
      ServerErrorCodes.notImplementDatabaseGetVersionMethod,
    );
  }

  public async getMigrations(
    dbVersion: string,
    currentVersion: string,
    type: MigrationType,
  ): Promise<Array<MigrationData>> {
    throw new NotImplementMethod(
      ServerErrorCodes.notImplementDatabaseGetMigrationsMethod,
    );
  }

  public async setVersion(
    version: string,
    type: MigrationType,
  ): Promise<void> {
    throw new NotImplementMethod(
      ServerErrorCodes.notImplementDatabaseSetVersionMethod,
    );
  }

  public async deleteVersion(
    version: string,
    type: MigrationType,
  ): Promise<void> {
    throw new NotImplementMethod(
      ServerErrorCodes.notImplementDatabaseDeleteVersionMethod,
    );
  }

  protected static async load(migrationPath: string): Promise<typeof BaseMigration> {
    const loaded = await import(migrationPath);
    const className = Object.keys(loaded)
      .filter((key) => key)
      .pop();

    return loaded[className as string] as typeof BaseMigration;
  }

  private static async delay(): Promise<void> {
    return new Promise(
      (resolve) => setTimeout(() => resolve(), 5000),
    );
  }
}
