import path from 'path';

import { Pool, PoolClient } from 'pg';
import glob from 'glob';

import { Config } from '../../interfaces/config';
import { Logger } from '../../logger/logger';

import { Direction, MigrationType } from '../migrations/constants';
import { MigrationData } from '../migrations/interfaces';

import { Database as DatabaseInterface, Migration } from '../interfaces';
import { BaseDatabase } from '../index';
import { DBClientNotInitialized } from '../../errors/db-client-not-initialized';

export class Postgres extends BaseDatabase
  implements DatabaseInterface<PoolClient> {
  private readonly pool: Pool;

  private client?: PoolClient;

  constructor(config: Config, logger: Logger) {
    super(config, logger);
    this.logger = logger.getLogger('postgres-client');

    this.logger.info('create postgres pool');
    this.pool = new Pool({
      host: config.db.pg.host,
      port: config.db.pg.port,
      database: config.db.pg.db,
      user: config.db.pg.user,
      password: config.db.pg.password,
    });
  }

  public getClient(): PoolClient | undefined {
    return this.client;
  }

  public async close(): Promise<void> {
    this.logger.info('disconnecting to postgres');
    await this.pool.end();
    this.logger.info('disconnected to postgres');
  }

  public async init(): Promise<void> {
    const client = await this.pool.connect();

    try {
      await client.query('BEGIN');
      await client.query(
        `
            CREATE TABLE IF NOT EXISTS t_migrations
            (
                id         SERIAL CONSTRAINT t_migrations_pk PRIMARY KEY,
                version    VARCHAR(60) NOT NULL,
                created_at BIGINT NOT NULL
            );
        `,
      );
      await client.query(
        `
            CREATE TABLE IF NOT EXISTS t_seeds
            (
                id         SERIAL CONSTRAINT t_seeds_pk PRIMARY KEY,
                version    VARCHAR(60) NOT NULL,
                created_at BIGINT NOT NULL
            );
        `,
      );
      await client.query('COMMIT');
      this.client = client;
    } catch (e) {
      await client.query('ROLLBACK');
    }
  }

  public async getVersion(type: MigrationType): Promise<string> {
    if (!this.client) {
      throw new DBClientNotInitialized();
    }

    let table = 't_migrations';

    if (type === MigrationType.seed) {
      table = 't_seeds';
    }

    const result = await this.client.query<Migration>(
      `
        SELECT * 
        FROM ${table} 
        ORDER BY version DESC
        LIMIT 1;
      `,
    );

    if (result.rows.length === 0) {
      return '';
    }

    return result.rows[0].version;
  }

  public async getMigrations(
    dbVersion: string,
    currentVersion: string,
    type: MigrationType,
  ): Promise<Array<MigrationData>> {
    if (dbVersion === currentVersion) {
      return [];
    }

    let direction = Direction.up;

    if (dbVersion > currentVersion) {
      direction = Direction.down;
    }

    const rootPath = path.resolve(
      path.join(
        __dirname,
        '..',
        'migrations',
        type === MigrationType.migrate ? 'postgres' : 'seed',
      ),
    );
    const files = await new Promise<Array<string>>(
      (resolve, reject) => {
        glob(
          '*.@(ts|js)',
          {
            cwd: rootPath,
          },
          (err, items) => {
            if (err) {
              return reject(err);
            }

            return resolve(items);
          },
        );
      },
    );

    const migrationClasses = await Promise.all<MigrationData>(
      files.map(async (file) => {
        const Class = await Postgres.load(path.join(
          rootPath,
          file,
        ));
        const instance = new Class(this, this.logger, this.config.app);

        return {
          instance,
          direction,
          version: instance.getName(),
        };
      }),
    );

    return migrationClasses.filter((migration) => {
      switch (true) {
      case direction === Direction.up
        && migration.version > dbVersion
        && migration.version <= currentVersion:
        return true;
      case direction === Direction.down
        && migration.version <= dbVersion
        && migration.version > currentVersion:
        return true;
      default:
        return false;
      }
    });
  }

  public async setVersion(
    version: string,
    type: MigrationType,
  ): Promise<void> {
    if (!this.client) {
      throw new DBClientNotInitialized();
    }

    let table = 't_migrations';

    if (type === MigrationType.seed) {
      table = 't_seeds';
    }

    await this.client.query(
      `
        INSERT INTO ${table}(version, created_at)
        VALUES($1, $2);
      `,
      [
        version,
        +new Date(),
      ],
    );
  }

  public async deleteVersion(
    version: string,
    type: MigrationType,
  ): Promise<void> {
    if (!this.client) {
      throw new DBClientNotInitialized();
    }

    let table = 't_migrations';

    if (type === MigrationType.seed) {
      table = 't_seeds';
    }

    await this.client.query(
      `
          DELETE FROM ${table}
          WHERE version = $1;
      `,
      [
        version,
      ],
    );
  }
}
