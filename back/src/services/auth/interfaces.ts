/* eslint-disable no-unused-vars */
export interface SignInParams {
  userName: string;
  password: string;
}

export interface SignUpParams {
  userName: string;
  password: string;
}

export interface verifiedResponse {
  status: string;
  error: string;
  id: string;
}

export interface VerifyParams {
  token: string;
  id?: string;
}
