import { Logger as PinoLogger } from 'pino';

import { Repositories } from '../../repositories';

import { Jwt } from '../../libraries/jwt';

import { Bcrypt } from '../../libraries/bcrypt';

import {
  Dashboard,
  Params,
} from '../interfaces';
import { MyPageParams } from './interfaces';
import { Payment as PaymentDomain } from '../../domains/user';

export class DashboardService implements Dashboard {
  private readonly logger: PinoLogger;

  private readonly repositories: Repositories;

  private readonly jwt: Jwt;

  private readonly bcrypt: Bcrypt;

  constructor(params: Params) {
    this.logger = params.logger.getLogger('documents-service');
    this.repositories = params.repositories;
    this.jwt = params.jwt;
    this.bcrypt = params.bcrypt;
  }

  async myPage(params: MyPageParams): Promise<Array<PaymentDomain> | undefined> {
    const payments = await this.repositories.payments.getAll();
    return payments;
  }
}
