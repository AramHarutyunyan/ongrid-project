import {
  Params,
  Auth,
  Dashboard,
} from './interfaces';
import { AuthService } from './auth';
import { DashboardService } from './dashboard';

export class Services {
  public readonly auth: Auth;

  public readonly dashboard: Dashboard;

  constructor(params: Params) {
    this.auth = new AuthService(params);
    this.dashboard = new DashboardService(params);
  }
}
