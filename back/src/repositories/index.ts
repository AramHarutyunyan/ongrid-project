import { Logger } from '../libraries/logger/logger';

import {
  Payments,
  Users,
} from './interfaces';

import { Postgres } from '../libraries/database/pg';
import { UserPostgresRepository } from './users/postgres';
import { PaymentsPostgresRepository } from './payments/postgres';

export class Repositories {
  public readonly users: Users;

  public readonly payments: Payments;

  constructor(
    postgres: Postgres,
    logger: Logger,
  ) {
    const db = postgres.getClient();

    if (!db) {
      throw new Error('pg client is undefined');
    }

    this.users = new UserPostgresRepository(
      db,
      logger,
    );

    this.payments = new PaymentsPostgresRepository(
      db,
      logger,
    );
  }
}
