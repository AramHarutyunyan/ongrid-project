/* eslint-disable camelcase */
export interface Payment {
  id: bigint;
  name: string;
  date: string;
  shipto: string;
  paymentmethod: string;
  saleamount: string;
  created_at: bigint;

}
