import { Logger as PinoLogger } from 'pino';
import { PoolClient } from 'pg';

import { Logger } from '../../libraries/logger/logger';

import { Payment as PaymentDomain } from '../../domains/user';

import { Payments } from '../interfaces';
import { Payment } from './interfaces';

export class PaymentsPostgresRepository implements Payments {
  private readonly logger: PinoLogger;

  private readonly client: PoolClient;

  constructor(client: PoolClient, logger: Logger) {
    this.client = client;
    this.logger = logger.getLogger('documents-repository');
  }

  public async getAll(): Promise<Array<PaymentDomain> | undefined> {
    const result = await this.client
      .query<Payment>(
        `
          SELECT *
          FROM payments
        `,
        [],
      );

    if (result.rows.length === 0) {
      return undefined;
    }

    return result.rows.map((data) => PaymentsPostgresRepository.mapper(data));
  }

  private static mapper(raw: Payment): PaymentDomain {
    const result: PaymentDomain = {
      id: raw?.id,
      name: raw?.name,
      date: raw.date,
      shipTo: raw.shipto,
      paymentMethod: raw.paymentmethod,
      saleAmount: raw.saleamount,
      createdAt: new Date(parseInt(String(raw.created_at), 10)),
    };

    return result;
  }
}
